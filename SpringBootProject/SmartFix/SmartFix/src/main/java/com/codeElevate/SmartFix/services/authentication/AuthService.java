package com.codeElevate.SmartFix.services.authentication;

import com.codeElevate.SmartFix.dto.SignUpRequestDTO;


import com.codeElevate.SmartFix.dto.UserDto;
import org.springframework.stereotype.Service;


@Service
public interface AuthService {
	
	 UserDto signupTenant(SignUpRequestDTO  signUpRequestDTO);
	 Boolean presentByEmail(String email);
	 UserDto signupOwner(SignUpRequestDTO  signUpRequestDTO);

}
