package com.codeElevate.SmartFix.services.authentication;

import org.springframework.beans.factory.annotation.Autowired;


import org.springframework.stereotype.Service;

import com.codeElevate.SmartFix.dto.SignUpRequestDTO;
import com.codeElevate.SmartFix.dto.UserDto;
import com.codeElevate.SmartFix.entity.User;
import com.codeElevate.SmartFix.enums.UserRole;
import com.codeElevate.SmartFix.repository.UserRepository;

@Service
public class AuthServiceImpl implements AuthService{

	 @Autowired
	 private UserRepository userRepository;
	 
	 public UserDto signupTenant(SignUpRequestDTO  signUpRequestDTO){
		 User user = new User();
		 
		 user.setName(signUpRequestDTO.getName());
		 user.setLastname(signUpRequestDTO.getLastname());
		 user.setEmail(signUpRequestDTO.getEmail());
		 user.setPhone(signUpRequestDTO.getPhone());
		 user.setPassword(signUpRequestDTO.getPassword());
		 
		 user.setRole(UserRole.TENANT);
		 return userRepository.save(user).getDto(); 
	 }
	 
	 public Boolean presentByEmail(String email){
		 return userRepository.findFirstByEmail(email) != null;
	 }
	 
	 
	 public UserDto signupOwner(SignUpRequestDTO  signUpRequestDTO){
		 User user = new User();
		 
		 user.setName(signUpRequestDTO.getName());
		 user.setEmail(signUpRequestDTO.getEmail());
		 user.setPhone(signUpRequestDTO.getPhone());
		 user.setPassword(signUpRequestDTO.getPassword());
		 
		 user.setRole(UserRole.OWNER);
		 return userRepository.save(user).getDto(); 
	 }
}
