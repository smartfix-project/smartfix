package com.codeElevate.SmartFix.controller;

import org.springframework.beans.factory.annotation.Autowired;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.codeElevate.SmartFix.dto.SignUpRequestDTO;
import com.codeElevate.SmartFix.dto.UserDto;
import com.codeElevate.SmartFix.services.authentication.AuthService;

@RestController
public class AuthenticationController {
	
	@Autowired
	private AuthService authService;
	
	@PostMapping("/tenant/sign-up")
	public ResponseEntity<?> signUpTenant(@RequestBody SignUpRequestDTO  signUpRequestDTO){
		
		if(authService.presentByEmail(signUpRequestDTO.getEmail())){
			return new ResponseEntity<>( "Tenant already exists with this email",HttpStatus.NOT_ACCEPTABLE);
		}
		
		UserDto createdUser = authService.signupTenant(signUpRequestDTO);
		 return new ResponseEntity<>(createdUser, HttpStatus.OK);
	}
	
	@PostMapping("/owner/sign-up")
	public ResponseEntity<?> signUpOwner(@RequestBody SignUpRequestDTO  signUpRequestDTO){
		
		if(authService.presentByEmail(signUpRequestDTO.getEmail())){
			return new ResponseEntity<>( "Owner already exists with this email",HttpStatus.NOT_ACCEPTABLE);
		}
		
		UserDto createdUser = authService.signupTenant(signUpRequestDTO);
		 return new ResponseEntity<>(createdUser, HttpStatus.OK);
	}

}
