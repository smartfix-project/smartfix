package com.codeElevate.SmartFix;

import org.springframework.boot.SpringApplication;

import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmartFixApplication {

	public static void main(String[] args) {
		SpringApplication.run(SmartFixApplication.class, args);
	}

}
