package com.codeElevate.SmartFix.dto;

import lombok.Data;

@Data
public class SignUpRequestDTO {
private Long id;
	
	private String email;

	private String password;
	
	private String name;
	
	private String lastname;
	
	private String phone;
	
	public SignUpRequestDTO() {
		
	}

	public SignUpRequestDTO(Long id, String email, String password, String name, String lastname, String phone) {
		super();
		this.id = id;
		this.email = email;
		this.password = password;
		this.name = name;
		this.lastname = lastname;
		this.phone = phone;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	


}
