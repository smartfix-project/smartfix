package com.codeElevate.SmartFix.enums;

public enum UserRole {
    TENANT,
    OWNER
}
